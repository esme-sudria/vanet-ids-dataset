## Dataset description

This page describes our dataset used to applicate our methodology to achieve and deploy an ML-based IDS. Our objective is evaluation of misbehavior detection mechanisms for VANETs. This GIT reposory consists of the used ressources (scripts and generated data). TThis dataset is shared to provide a baseline for the comparison of detection mechanisms using supervised ML techniques. It helps also
 to serve as a starting point for more sophisticated VANET attacks. The dataset is part of a contributon submited to IEEE magazine in 2023.


## Creation procedure

To create the dataset, we used the simulators SUMO, OMNET++, and VEINS. Figure \ref{figarchidata} illustrates our proposed work diagram to generate a synthetic dataset in the VANET environment.


<img src="ressources/archite.png" width="600">


As shown in previous Figure, we began by using SUMO to generate the traffic, exporting the Luxembourg traffic scenario (LuST). The SUMO mobility traffic was then imported into the OMNET++ simulator to generate network traffic, including normal and malicious behavior. 

In OMNET++, we also imported two frameworks: INET and VEINS. VEINS utilizes protocols and applications provided by INET to simulate normal and malicious traffic. Our work considers the following types of attacks: constant position, constant position offset, random position, random position offset, constant speed, constant speed offset, random speed, and random speed offset. The simulation parameters are summarized in next Table.

<img src="ressources/simu_params.PNG" width="400">
 
We conducted 8 simulations with varying traffic densities and attack densities. The result of simulation consists into two files: log files and a ground truth file. The log files correspond to every vehicle in the simulation. 

Each log file contains received messages from other vehicles (e.g., position, speed, message ID). The ground truth file indicates the attacker's behavior. The raw data collected from the simulations is then ready for preparation and processing.

The final file contains 933319 records. In each record, there are 11 features as reported in next Table :

<img src="ressources/features_params.PNG" width="500">


 
## Repertory description
The structure of the repository is as follows:

   - `data`: Open dataset containing the simulated data.

   - `scripts`: Utility scripts to generate data.
   
   Please refer to the `README` files in the subfolders for more information and licences.
   
   


## Acknowledge
   If you use the proposed open traces, please follow the aknowledgement rules in the `README` files in the subfolders.
   The dataset was primarily put together by Hela Marouane and Lamine Amour at the ESME Research Lab, part of ESME ingenerring school. Please contact Hela and/or Lamine if you have any questions, comments or criticism. 
   
   
    
## Related publication - Paper to cite
   A review and a tutorial of ML-based MDS technology within a VANET context: From data collection to trained model deployment.  Marouane, H.; Dandoush, A.; Amour, L. and Erbad, A. IEEE Magazine. pages 1-14, 2023. 

## Authors
- Hela Marouane (Modeling & Digital Technologies department - Engineering school - ESME) 

- Abdulhalim Dandoush (University of Doha for Science and Technology) 

- Lamine Amour (Modeling & Digital Technologies department - Engineering school - ESME) 

- Aiman Erbad  (University of Doha for Science and Technology) 


